SHELL := /bin/bash

DEPENDENCIES := venv/dependencies.timestamp
PACKAGE := bw-menu
VENV := venv/venv.timestamp
VERSION := $(shell python3 -c 'import bw_menu; print(bw_menu.__version__)')
BUILD_DIR := dist_$(VERSION)
BUILD := $(BUILD_DIR)/.build.timestamp

all: static-analysis test

$(VENV):
	python3 -m venv venv
	. venv/bin/activate
	touch $(VENV)

$(DEPENDENCIES): $(VENV) requirements-make.txt requirements.txt
	# Install Python dependencies, runtime *and* test/build
	./venv/bin/python3 -m pip install --requirement requirements-make.txt
	./venv/bin/python3 -m pip install --requirement requirements.txt
	touch $(DEPENDENCIES)

.PHONY: static-analysis
static-analysis: $(DEPENDENCIES)
	pylint bw_menu/ tests/
	mypy bw_menu/ tests/
	black --check bw_menu/ tests/

.PHONY: test
test: $(DEPENDENCIES)
	pytest tests/

.PHONY: test-verbose
test-verbose: $(DEPENDENCIES)
	pytest  -rP --log-cli-level=10 tests/

fix: $(DEPENDENCIES)
	# Enforce style with Black
	black bw_menu/
	black tests/

.PHONY: package
package: $(BUILD)

$(BUILD): $(DEPENDENCIES)
	# Build the package
	@if grep --extended-regexp "^ *(Documentation|Bug Tracker|Source|url) = *$$" "setup.cfg"; then \
		echo 'FAILURE: Please fully fill out the values for `Documentation`, `Bug Tracker`, `Source`, and `url` in `setup.cfg` before packaging' && \
		exit 1; \
		fi
	mkdir --parents $(BUILD_DIR)
	./venv/bin/python3 -m build --outdir $(BUILD_DIR)
	touch $(BUILD)

.PHONY: version
version:
	@printf $(VERSION)'\n'

.PHONY: publish
publish: package test
	@test $${TWINE_PASSWORD?Please set environment variable TWINE_PASSWORD in order to publish}
	./venv/bin/python3 -m twine upload --username __token__ $(BUILD_DIR)/*

.PHONY: publish-test
publish-test: package test
	@test $${TWINE_PASSWORD?Please set environment variable TWINE_PASSWORD in order to publish}
	./venv/bin/python3 -m twine upload --repository testpypi --username __token__ $(BUILD_DIR)/*
